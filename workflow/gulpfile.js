// ==========================
// required plugins
// ==========================
var gulp         = require('gulp');
var sass         = require('gulp-sass');
var postcss      = require('gulp-postcss');
var autoprefixer = require('autoprefixer');
var cssnano      = require('cssnano');
var concat       = require('gulp-concat');
var jshint       = require('gulp-jshint');
var gulpCopy     = require('gulp-copy');
var newer        = require('gulp-newer');
var imagemin     = require('gulp-imagemin');
var pngquant     = require('imagemin-pngquant');
var uglify       = require('gulp-uglify');
var sourcemaps   = require('gulp-sourcemaps');
var runSequence  = require('run-sequence');
var browserSync  = require('browser-sync').create();
var watch        = require('gulp-watch');

// ==========================
// default tasks configurations
// ==========================

// browser sync configuration
gulp.task('browserSync', function() {
  browserSync.init({
    proxy: "sirius-madness.dev"
    });
  });

// sass configuration
gulp.task('sass', function() {
  return gulp.src('../assets/styles/**/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
    //.pipe(autoprefixer('last 2 version'))
    .pipe(sourcemaps.write('../assets/sourcemaps'))
    .pipe(gulp.dest('../site/assets'))
    .pipe(browserSync.stream({match: '../site/assets/**/*.css'}));
  });

// javascript concatenation configuration
gulp.task('concat', function() {
  return gulp.src('../assets/js/plugins/**/*.js')
    .pipe(sourcemaps.init())
    .pipe(concat('scripts.js'))
    .pipe(sourcemaps.write('../assets/sourcemaps'))
    .pipe(gulp.dest('../site/assets'))
    .pipe(browserSync.stream({match: '../site/assets/**/*.js'}));
  });

// image minification configuration
gulp.task('imagemin', function() {
  return gulp.src('../assets/images/**/*')
  .pipe(imagemin({
    progressive: true,
    svgoPlugins: [{removeViewBox: false}],
    use: [pngquant()]
    }))
  .pipe(gulp.dest('../site/assets/images'));
  });

// copy icons configuration
gulp.task('copyIcons', function() {
  return gulp.src('../assets/icons/**/*')
  .pipe(newer('../site/assets/icons'))
  .pipe(gulp.dest('../site/assets/icons'))
  .pipe(browserSync.stream);
  });

// copy fonts configuration
gulp.task('copyFonts', function() {
  return gulp.src('../assets/fonts/**/*')
  .pipe(newer('../site/assets/fonts'))
  .pipe(gulp.dest('../site/assets/fonts'))
  .pipe(browserSync.stream);
  });

// ==========================
// build tasks configurations
// ==========================

// css minification configuration
gulp.task('cssnano', function() {
  return gulp.src(stylesDest)
    .pipe(sourcemaps.init())
    .pipe(cssnano())
    .pipe(sourcemaps.write(stylesMap))
    .pipe(gulp.dest(stylesDest));
  });

// javascript minification configuration
gulp.task('uglify', function() {
  return gulp.src(scriptsSrc)
    .pipe(sourcemaps.init())
    .pipe(uglify())
    .pipe(sourcemaps.write(scriptsMap))
    .pipe(gulp.dest(scriptsDest));
  });


// watch configuration
gulp.task('watch', function() {
  gulp.watch('../assets/styles/**/*.scss', ['sass'], browserSync.reload);
  gulp.watch('../assets/js/plugins/**/*.js', ['concat'], browserSync.reload);
  gulp.watch('../site/**/*.md').on('change', browserSync.reload);
  gulp.watch('../site/**/*.php').on('change', browserSync.reload);
});

gulp.task('default', function(callback) {
  runSequence(['sass', 'concat', 'imagemin', 'browserSync'], 'watch',
  callback)
  });

gulp.task('build', function(callback) {
  runSequence(['cssnano', 'uglify'],
      callback)
  });


