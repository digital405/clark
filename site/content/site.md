title: The Inn at Clark's
----
description: Our goal is to make your stay as comfortable as possible. We feel like our guest are family. The Inn at Clark's provides all the extra conveniences to make your trip hassle free.
----
tagline: The Best Kept Secret on the Texas Cost
----
keywords: vrbo texas, texas coast, surfside beach hotel, surfside beach hotels, texas beaches resorts, texas beach resort, gulf coast rentals, matagorda tx beach, texas coastal vacation, port o'conner hotel, port o'conner resort
----
