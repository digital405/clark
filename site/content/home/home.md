title: The Inn at Clark's
----
description: Description
----
text:

## Welcome to Our Inn

Nestled inside the intercoastal waterway of Port O’ Connor, Texas is the rustic "The Inn at Clark's". The Inn at Clark's provides an easy access to the beautiful Gulf of Mexico while offering several types of rooms to make your vacation as enjoyable as possible. 

All rooms have a queen sized beds, television, WIFI, air conditioning & heating, local telephone service, balcony area, private bath, and continental breakfast. The Inn at Clark’s offers several types of rooms suited for a variety of needs.

Amenities include coffee pots in room, hair dryers, ice machines, boat slips, covered parking, tackle box closet with lock, and free continental breakfast. Port O’ Connor is “The Best Kept Fishing Secret on the Texas Coast”! After a day at the beach, fishing, or just enjoying your vacation, book your stay at the Inn.


If your interests include fishing, swimming, bird watching, tournaments, sandcastle building, or visiting the famous Matagorda Lighthouse, Port O’ Connor offers activities for the whole family. When planning your vacation, visit our “Fisherman’s Best Kept Secret” and enjoy our reasonable rates. We are looking forward to seeing you!
